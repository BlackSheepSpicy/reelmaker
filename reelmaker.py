#! /usr/bin/env python3

import xlrd,requests,youtube_dl,os,logging
import moviepy.editor as mov
from sys import argv

#log init
logging.basicConfig(filename='ClipCutterLog.txt',filemode='a',level=logging.DEBUG)
logging.getLogger().addHandler(logging.StreamHandler())

#converts clip cut points to seconds, moviepy only takes time values in seconds
def ToSeconds(time):
	conChart=[3600,60,1]
	seconds=0
	i=0
	for entries in time:
		seconds+=conChart[i]*time[i]
		i+=1
	return seconds

def Main(cutsheet):
	#Enumerates over excel spreadsheet to gather vod link and clip times
	#sets default values for common missing parameters (clip title and clip end time)
	logging.info('Clip Cut Initialized')	
	logging.info('Parsing spreadsheet')
	sheet=xlrd.open_workbook(cutsheet)
	spreadsheet=sheet.sheet_by_index(0)

	vodLink=spreadsheet.cell(1,0).value
	logging.debug(f'vod: {vodLink}')
	timeBuffer=spreadsheet.cell(1,1).value
	logging.debug(f'Time Buffer: {timeBuffer}')
	includeClips=spreadsheet.cell(1,2).value
	logging.debug(f'Include Clips: {includeClips}')
	
	defaultClipLen=8
	cutList=[]
	untitledClips=1
	logging.info(f'total clips detected: {spreadsheet.nrows-3}')
	for row in range(3,spreadsheet.nrows):
		logging.debug(f'parsing clip {row-2} parameters')
		cutParams=[]
		negBuffer=True
		for column in range(0,3):
			value=spreadsheet.cell(row,column).value
			if type(value) == float:
				value=xlrd.xldate_as_tuple(value,sheet.datemode)
				if negBuffer:
					logging.debug(f'applying negative buffer to column: {column}')
					value = ToSeconds(value[3:])-timeBuffer
					negBuffer = False
				else:
					logging.debug(f'applying positive buffer to column: {column}')
					value = ToSeconds(value[3:])+timeBuffer
			if column == 0 and value == '':
				logging.warning(f'unnamed clip in column: {column}')
				value=f'Untitled Clip {untitledClips}'
				untitledClips+=1
			if column == 0 and '/' in value:
				logging.warning(f'forward slash found in column: {column}')
				value=value.replace('/',':')			
			if column == 2 and value == '':
				logging.warning(f'end of clip "[cutParams[0]" not specified, falling back on default clip length of {defaultClipLen} seconds')
				value=cutParams[1]+defaultClipLen
			cutParams.append(value)
		logging.debug(cutParams)
		cutList.append(cutParams)
	logging.info('spreadsheet parsing complete')

	#checks if same vod from last exec is being used
	#prevents redownload (saves hella exec time)
	if not '--ignore-prevvod' in argv:
		logging.info('checking previous vod')
		if not os.path.exists('PrevVod'):
			logging.warning('vod log does not exist, creating')
			open('PrevVod','w').close()
		with open('PrevVod') as f:
			if not f.read() == vodLink:
				logging.info('new vod detected, fetching')
				if os.path.exists('vod.mp4'):
					os.remove('vod.mp4')
				youtube_dl.YoutubeDL({'outtmpl':'vod.%(ext)s'}).download([vodLink])
				logging.info('vod fetch successful')
			else:
				logging.info('vod is the same as previous run')
		with open('PrevVod','w') as f:
			f.write(vodLink)
	else:
		logging.info('ignoring previous vod')

	logging.info('beginning highlight reel creation')
	vod=mov.VideoFileClip('vod.mp4')
	reelName=cutsheet.replace('.xlsx','.mp4')
	if 'yes' in includeClips:
		logging.debug('creating directories')
		os.mkdir('Deliverables')
		os.mkdir('Deliverables/Cuts')
		reelName='Deliverables/'+reelName
		logging.debug('directory creation successful')
	
	logging.debug('beginning cut operations')
	clips=[]
	for entry in cutList:
		logging.debug(f'Creating clip {cutList.index(entry)+1} of {len(cutList)}: {entry}')
		clipFinal=vod.subclip(entry[1],entry[2])
		clips.append(clipFinal)
		if 'yes' in includeClips:
			clipFinal.write_videofile(f'Deliverables/Cuts/{entry[0]}.mp4',fps=30,audio_codec='aac')
	logging.debug('cut operations complete')
	logging.debug('creating final highlight reel')
	mov.concatenate_videoclips(clips).write_videofile(reelName,fps=30,audio_codec='aac')

	if 'yes' in includeClips:
		logging.debug('highlight reel complete, compressing')
		os.system(f'zip -r {cutsheet}.zip Deliverables')
	logging.info('all operations completed, exiting')

Main(argv[1])
